class Train < ActiveRecord::Base
	has_many :routes, through: :scheduled_services
	has_many :scheduled_services

  structure do
  		model_name		"British Rail 153 Series", :validates => :presence
  		registration_number "CA12512",			:validates => { :format => /\A\w+$\z/ }
    	maximum_speed	120 #kilometers per hour
    	capacity		420 #Paseengers (sitting and standing)
    timestamps
  end
end


# Note: /\A\w+$\z/ was used instead of /^\w+$/ due to a security issue

