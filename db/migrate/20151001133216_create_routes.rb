class CreateRoutes < ActiveRecord::Migration
  def self.up
    create_table :routes do |t|
      t.datetime :updated_at 
      t.datetime :created_at 
    end
  end
  
  def self.down
    drop_table :routes
  end
end
