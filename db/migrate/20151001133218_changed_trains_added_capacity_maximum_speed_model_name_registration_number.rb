class ChangedTrainsAddedCapacityMaximumSpeedModelNameRegistrationNumber < ActiveRecord::Migration
  def self.up
    add_column :trains, :capacity, :integer
    add_column :trains, :maximum_speed, :integer
    add_column :trains, :model_name, :string
    add_column :trains, :registration_number, :string
  end
  
  def self.down
    remove_column :trains, :capacity
    remove_column :trains, :maximum_speed
    remove_column :trains, :model_name
    remove_column :trains, :registration_number
  end
end
