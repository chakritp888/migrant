class CreateScheduledServices < ActiveRecord::Migration
  def self.up
    create_table :scheduled_services do |t|
      t.datetime :updated_at 
      t.datetime :created_at 
    end
  end
  
  def self.down
    drop_table :scheduled_services
  end
end
